"use server";

import { Resend } from "resend";

const resend = new Resend(process.env.RESEND_API_KEY);

export default async (req, res) => {
  if (req.method === "POST") {
    // Destructuring to include email, phone, and sport from the request body
    const { name, company, email, phone } = req.body;
    const emailBody = `New enquiry from:
      Name: ${name}
      Company: ${company}
      Email: ${email}
      Phone: ${phone}`;
    resend.emails
      .send({
        from: "Thelios Enquiry <info@sportsseam.com>",
        to: [
          "razin.tailor@sportsseam.com",
          "rajiv.chamraj@sportsseam.com",
          "john.coyle@sportsseam.com",
        ],
        subject: "New Enquiry",
        text: emailBody,
        reply_to: email,
      })
      .then(() => {
        res.status(200).json({
          message:
            "Thank you for your interest in Thelios. We'll reach out to you shortly.",
        });
      })
      .catch((e) => {
        console.error(e); // Log the error for server-side debugging.
        res.status(500).json({ error: "Failed to send email." });
      });
  } else {
    // Handle non-POST requests
    res.setHeader("Allow", ["POST"]);
    res.status(405).end(`Method ${req.method} Not Allowed`);
  }
};
