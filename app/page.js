import Hero from "./components/Hero";
import Overview from "./components/Overview";
import KnowledgeVisualistsSection from "./components/KnowledgeVisualist";
import AIActionSection from "./components/AIInAction";
import Saas from "./components/Saas";
import VideoAnalysisGrid from "./components/VideoAnalysisGrid";
import ComparisonChart from "./components/ComparisonChart";
import LearnMore from "./components/LearnMore";
import { BackgroundBeams } from "./components/ui/background-beams";
import ComingSoonBasketball from "./components/comingsoonbasketball";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col mx-auto max-w-7xl px-4 sm:px-6 lg:px-8 ">
      <Hero />
      {/* <Overview /> */}
      <KnowledgeVisualistsSection />
      <Saas />
      <VideoAnalysisGrid />
      <ComingSoonBasketball />
      <LearnMore />
      {/* <AIActionSection/> */}
    </main>
  );
}
