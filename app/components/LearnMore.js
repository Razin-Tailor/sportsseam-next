"use client";
import Image from "next/image";
import React from "react";

// components/LearnMore.js
import { useState } from "react";

export default function LearnMore() {
  const [formData, setFormData] = useState({
    name: "",
    company: "",
    email: "",
    phone: "",
    // sport: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    fetch("/api/sendEmail", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    })
      .then((res) => res.json()) // Parse JSON response
      .then((data) => {
        if (data.message) {
          alert(data.message); // Show success message
        } else if (data.error) {
          alert(data.error); // Show error message
        }
        setFormData({
          name: "",
          company: "",
          email: "",
          phone: "",
          // sport: "",
        });
      })
      .catch((e) => {
        console.error(e);
        alert("An error occurred while sending the email.");
      });
  };
  return (
    <div className="mt-10 py-8" id="learn-more">
      <div className="text-justify mb-8">
        <h4 className="text-3xl sm:text-4xl md:text-6xl font-light text-white">
          Learn More
        </h4>
      </div>
      <div className="flex flex-col md:flex-row justify-between items-center space-y-4 md:space-y-0 md:space-x-4">
        <div className="w-full md:flex-1 md:max-w-md lg:max-w-lg xl:max-w-xl 2xl:max-w-2xl">
          {" "}
          {/* Left column for the SVG, will take full width on mobile and specified max-width on larger screens */}
          <Image
            src="/learnmore.svg" // Replace with the path to your SVG
            alt="Descriptive text for the SVG"
            layout="responsive"
            width={500} // These are placeholder values, they define the aspect ratio
            height={500} // Adjust as needed for your image
          />
        </div>
        <div className="w-full md:flex-1">
          {" "}
          {/* Right column for the form, will take full width on mobile */}
          <form onSubmit={handleSubmit} className="space-y-4">
            <div>
              <label
                htmlFor="name"
                className="block text-sm font-medium text-white"
              >
                Name
              </label>
              <input
                type="text"
                name="name"
                id="name"
                required
                className="mt-1 p-2 block w-full bg-gray-700 text-white border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
                value={formData.name}
                onChange={handleChange}
              />
            </div>
            <div>
              <label
                htmlFor="company"
                className="block text-sm font-medium text-white"
              >
                Company
              </label>
              <input
                type="text"
                name="company"
                id="company"
                required
                className="mt-1 p-2 block w-full bg-gray-700 text-white border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
                value={formData.company}
                onChange={handleChange}
              />
            </div>
            <div>
              <label
                htmlFor="email"
                className="block text-sm font-medium text-white"
              >
                Email
              </label>
              <input
                type="email"
                name="email"
                id="email"
                required
                className="mt-1 p-2 block w-full bg-gray-700 text-white border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
                value={formData.email}
                onChange={handleChange}
              />
            </div>
            <div>
              <label
                htmlFor="phone"
                className="block text-sm font-medium text-white"
              >
                Phone
              </label>
              <input
                type="tel"
                name="phone"
                id="phone"
                required
                className="mt-1 p-2 block w-full bg-gray-700 text-white border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
                value={formData.phone}
                onChange={handleChange}
              />
            </div>
            <button
              type="submit"
              className="mt-4 w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-lg font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              Learn More
            </button>
          </form>
        </div>
      </div>
    </div>
  );
  //   return (
  //     <div className="max-w-7xl mt-10 py-8" id="learn-more">
  //       <h4 className="text-4xl md:text-6xl font-light mb-8 text-white">
  //         Learn More
  //       </h4>
  //       <form onSubmit={handleSubmit} className="space-y-4">
  //         <div>
  //           <label
  //             htmlFor="name"
  //             className="block text-sm font-medium text-white"
  //           >
  //             Name
  //           </label>
  //           <input
  //             type="text"
  //             name="name"
  //             id="name"
  //             required
  //             className="mt-1 p-2 block w-full bg-gray-700 text-white border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
  //             value={formData.name}
  //             onChange={handleChange}
  //           />
  //         </div>
  //         <div>
  //           <label
  //             htmlFor="company"
  //             className="block text-sm font-medium text-white"
  //           >
  //             Company
  //           </label>
  //           <input
  //             type="text"
  //             name="company"
  //             id="company"
  //             required
  //             className="mt-1 p-2 block w-full bg-gray-700 text-white border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
  //             value={formData.company}
  //             onChange={handleChange}
  //           />
  //         </div>
  //         <div>
  //           <label
  //             htmlFor="email"
  //             className="block text-sm font-medium text-white"
  //           >
  //             Email
  //           </label>
  //           <input
  //             type="email"
  //             name="email"
  //             id="email"
  //             required
  //             className="mt-1 p-2 block w-full bg-gray-700 text-white border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
  //             value={formData.email}
  //             onChange={handleChange}
  //           />
  //         </div>
  //         <div>
  //           <label
  //             htmlFor="phone"
  //             className="block text-sm font-medium text-white"
  //           >
  //             Phone
  //           </label>
  //           <input
  //             type="tel"
  //             name="phone"
  //             id="phone"
  //             required
  //             className="mt-1 p-2 block w-full bg-gray-700 text-white border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
  //             value={formData.phone}
  //             onChange={handleChange}
  //           />
  //         </div>
  //         {/* <div>
  //           <label
  //             htmlFor="sport"
  //             className="block text-sm font-medium text-white"
  //           >
  //             Sport of Interest
  //           </label>
  //           <select
  //             name="sport"
  //             id="sport"
  //             required
  //             className="mt-1 block w-full p-2 bg-gray-700 text-white border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
  //             value={formData.sport}
  //             onChange={handleChange}
  //           >
  //             <option value="">Select a sport</option>
  //             <option value="NFL">NFL (Football)</option>
  //             <option value="Volleyball">Volleyball</option>
  //             <option value="Volleyball & NFL">Both</option>
  //           </select>
  //         </div> */}
  //         <button
  //           type="submit"
  //           className="mt-4 w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
  //         >
  //           Learn More
  //         </button>
  //       </form>
  //     </div>
  //   );
}
