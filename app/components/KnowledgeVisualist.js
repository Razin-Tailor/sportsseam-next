import React from "react";
import { TheliosLogoWithUnderline } from "./ui/theliosLogo";

const KnowledgeVisualistsSection = () => {
  const improvements = [
    {
      icon: "/list/automate.svg",
      text: "Automates game & player tracking - all from video",
      descriptions: ["No need for special cameras or other wearable devices"],
    },
    {
      icon: "/list/metrics.svg",
      text: "Custom analysis that aligns with your goals & objectives",
      descriptions: [
        "Configurable interface that allows coaches and analysts to define their visual analysis criteria.",
        "Creates a custom, unique dataset - a goldmine for evaluating performance and predicting success.",
      ],
    },
    {
      icon: "/list/objective.svg",
      text: "Accurate & precise",
      descriptions: [
        "Industry leading computer vision accuracy… and getting better everyday!",
        "Expertise in identifying and tracking small objects and their interactions with great precision and granularity",
      ],
    },
  ];

  const ListItem = ({ icon, text, descriptions, isBlue }) => {
    const arrowColorClass = isBlue ? "sm:arrowBlue" : "sm:arrowWhite";
    const arrowPadding = isBlue ? "sm:pl-20 sm:py-10" : "";
    const titleColor = isBlue ? "text-blue-400" : "";
    const hoverStyle =
      " transform hover:scale-105 hover:shadow-custom transition duration-300 ease-in-out";
    // const hoverStyle =
    //   "hover:shadow-md hover:brightness-105 transition  duration-300 ease-in-out";

    return (
      <div
        className={`min-h-40 flex items-center text-lg p-4 ${hoverStyle} w-full h-auto min-w-full`}
      >
        <img src={icon} className="h-20 w-20 mr-4" alt="" />
        <div>
          <p className={`${titleColor} font-bold text-2xl`}>{text}</p>
          <ul className="text-sm pr-2">
            {descriptions.map((description, index) => (
              <li key={index}>{description}</li>
            ))}
          </ul>
        </div>
      </div>
    );
  };

  return (
    <section className="text-white bg-dark-500 p-8">
      <div className="flex flex-col md:flex-row items-center justify-center md:justify-start text-center md:text-left mb-4 md:mb-6">
        <TheliosLogoWithUnderline />
        <h2 className="text-3xl md:text-4xl lg:text-5xl xl:text-6xl font-semibold whitespace-nowrap">
          Visual Copilot for Sports
        </h2>
      </div>

      <div className="relative w-80 mx-auto">
        {" "}
        {/* w-80 is Tailwind's class for 20rem */}
        <div
          className="absolute bottom-0 left-0 right-0 h-px blur-sm"
          style={{
            width: "20rem",
            backgroundImage:
              "linear-gradient(to right, transparent, #667eea, transparent)",
          }}
        />
        <div
          className="absolute bottom-0 left-0 right-0 h-px"
          style={{
            width: "20rem",
            backgroundImage:
              "linear-gradient(to right, transparent, #667eea, transparent)",
          }}
        />
        <div
          className="absolute bottom-0 left-0 right-0 h-px blur-sm"
          style={{
            width: "20rem",
            backgroundImage:
              "linear-gradient(to right, transparent, #38b2ac, transparent)",
          }}
        />
        <div
          className="absolute bottom-0 left-0 right-0 h-px"
          style={{
            width: "20rem",
            backgroundImage:
              "linear-gradient(to right, transparent, #38b2ac, transparent)",
          }}
        />
      </div>
      {/* <h2 className="text-4xl md:text-6xl font-light mb-2">
        <span>
          <Image
            src={"/logo-thelios-hr.svg"}
            width={230}
            height={180}
            alt="logo"
          />
        </span>
        Visual Copilot for Sports
      </h2> */}
      <div className="max-w-7xl flex flex-col lg:flex-row justify-between">
        <div className="max-w-7xl">
          {improvements.map((item, index) => (
            <ListItem
              key={index}
              icon={item.icon}
              text={item.text}
              descriptions={item.descriptions}
              isBlue={true}
            />
          ))}
        </div>
      </div>
    </section>
  );
};

export default KnowledgeVisualistsSection;
