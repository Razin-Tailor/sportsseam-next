// "use client";
// import Image from "next/image";
// import React from "react";

// const Navbar = () => {
//   return (
//     <nav className="flex items-center justify-between flex-wrap bg-gray-950 px-6 py-1 shadow-slate-700 shadow-sm z-50">
//       <div className="flex items-center flex-shrink-0 text-white mr-6">
//         <Image
//           src={"/logo-thelios-hr.svg"}
//           width={230}
//           height={180}
//           alt="logo"
//         />
//       </div>
//       <div className="flex-row-reverse flex items-center w-auto">
//         <div>
//           <button
//             className=" hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full outline text-xs"
//             onClick={() => {
//               const element = document.getElementById("learn-more");
//               if (element) {
//                 element.scrollIntoView({ behavior: "smooth" });
//               }
//             }}
//           >
//             Contact Us
//           </button>
//         </div>
//       </div>
//     </nav>
//   );
// };

// export default Navbar;
"use client";
import Image from "next/image";
import React from "react";

const Navbar = () => {
  return (
    <nav className="flex items-center justify-between bg-gray-950 px-4 py-2 shadow-slate-700 shadow-sm z-50">
      <div className="flex items-center justify-start">
        <div className="flex-shrink-0">
          <Image
            src={"/logo-thelios-hr.svg"}
            width={225} // Adjusted for smaller screens
            height={180} // Maintaining aspect ratio
            alt="logo"
            priority // Loads the image immediately
          />
        </div>
      </div>
      <div className="flex items-center justify-end">
        <button
          className="hover:bg-blue-700 text-white font-bold py-2 px-4 text-xs rounded-full outline transition ease-in duration-200"
          onClick={() => {
            const element = document.getElementById("learn-more");
            if (element) {
              element.scrollIntoView({ behavior: "smooth" });
            }
          }}
        >
          Contact Us
        </button>
      </div>
    </nav>
  );
};

export default Navbar;
