import Image from "next/image";
import React from "react";

const AIActionSection = () => {
  return (
    <div className="py-12 text-white">
      <div className="mx-auto px-4 py-2">
        <div className="text-justify mb-10">
          <h2 className="text-4xl md:text-6xl font-light mb-10 text-justify text-white">
            A Breakthrough in Computer Vision
          </h2>
          <p className="text-xl text-white text-justify">
            We've solved one of the most important problems in computer vision,
            accurately identifying small objects in visual data.
          </p>
          <p className="mt-2 text-lg text-white text-justify">
            Our AI powered copilot is mastering the ability to pinpoint and
            track the interaction of small objects, actions, and humans.
          </p>
        </div>
        <div className="grid md:grid-cols-3 gap-4">
          {/* Replace src with paths to your video stills or image assets */}
          <div className="relative h-64">
            <Image
              src={"analytics.svg"}
              alt="AI in Action: Football Game Analysis"
              layout="fill"
              objectFit="contain"
              className="rounded-lg"
            />
            <p className="absolute bottom-2 left-2 text-white">
              Football Game Analysis
            </p>
          </div>
          <div className="relative h-64">
            <Image
              src={"fitness.svg"}
              alt="AI in Action: Volleyball Match Analysis"
              layout="fill"
              objectFit="contain"
              className="rounded-lg"
            />
            <p className="absolute bottom-2 left-2 text-white">
              Volleyball Match Analysis
            </p>
          </div>
          <div className="relative h-64">
            <Image
              src={"medicine.svg"}
              alt="AI in Action: Healthcare Procedure"
              layout="fill"
              objectFit="contain"
              className="rounded-lg"
            />
            <p className="absolute bottom-2 left-2 text-white">
              Healthcare Procedure
            </p>
          </div>
        </div>
        {/* CTA can be adjusted to link to a more detailed page or video demonstration */}
        {/* <div className="text-justify mt-8">
          <button className="bg-blue-600 text-white px-6 py-2 rounded-md font-medium hover:bg-blue-700 transition duration-300">
            See AI Copilot in Action
          </button>
        </div> */}
      </div>
    </div>
  );
};

export default AIActionSection;
