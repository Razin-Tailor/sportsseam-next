import React from "react";
import Image from "next/image";

export function TheliosLogoWithUnderline() {
  return (
    <div className="flex w-full md:w-auto md:flex-none items-center justify-center overflow-hidden px-4">
      <div className="w-1/2 md:w-auto">
        <Image
          src="/logo-thelios-hr-white.svg"
          layout="intrinsic"
          width={320} // Base width size
          height={210} // Base height size, keep the aspect ratio
          alt="logo"
        />
      </div>
    </div>
  );
}
