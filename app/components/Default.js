"use client"
import React, { useState } from 'react'

const Default = () => {
    const [email, setEmail] = useState('');

    const handleSubmit = async (event) => {
        
        event.preventDefault();
        fetch('/api/sendEmail', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email: email }),
        })
        .then((res) => res.json()) // Parse JSON response
        .then((data) => {
            if (data.message) {
                alert(data.message); // Show success message
            } else if (data.error) {
                alert(data.error); // Show error message
            }
            setEmail('');
        })
        .catch((e) => {
            console.error(e);
            alert("An error occurred while sending the email.");
        });
    };
    return (
        <div className="flex flex-col justify-center items-center h-screen w-full">
            <img src="/logo-thelios.svg" alt="Thelios" className="h-96 mb-5" /> {/* Enlarged logo */}
            <div className="text-center">
                <h1 className="text-xl md:text-3xl font-bold text-white">
                    AI-powered Computer Vision to transform decision making
                </h1>
                <p className="mt-3 text-base md:text-lg text-gray-50">
                    Learn more about our innovative solutions.
                </p>
                <form onSubmit={handleSubmit} className="mt-8">
                    <input
                        type="email"
                        required
                        autoFocus="true"
                        placeholder="Your email"
                        className="px-4 py-2 mr-4 border rounded-md text-black"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <button type="submit" className="px-6 py-2 border rounded-md bg-[#2269b2] text-white hover:bg-blue-700">
                        Learn More
                    </button>
                </form>
            </div>
        </div>

    )
}

export default Default
