// components/ComparisonChart.js
import Image from 'next/image';

// components/DottedCircle.js
const DottedCircle = ({ children }) => {
    return (
      <div className="relative m-4">
        <div className="absolute inset-0 flex items-center justify-center">
          <div className="w-64 h-64 rounded-full border-4 border-orange-300 border-dotted"></div>
        </div>
        <div className="relative flex items-center justify-center h-64">
          {children}
        </div>
      </div>
    );
  };
  

const ListItem = ({ iconSrc, title, description }) => {
  return (
    <div className="flex items-start mt-4">
      <Image src={iconSrc} width={24} height={24} alt={title} />
      <div className="ml-2">
        <h4 className="font-semibold">{title}</h4>
        <p className="text-sm text-gray-600">{description}</p>
      </div>
    </div>
  );
};

const ComparisonChart = ({ leftItems, rightItems }) => {
  return (
    <section className=" p-6">
      <h2 className="text-3xl font-bold text-white mb-10">Automating Visual Analysis to Improve Decision Making</h2>
      
      <div className="flex flex-col md:flex-row items-center justify-center">
        {/* Left column */}
        <div className="flex-1 flex flex-col items-center px-4">
          <DottedCircle>Current Visual Challenges</DottedCircle>
          {leftItems.map(item => (
            <ListItem key={item.title} {...item} />
          ))}
        </div>
        
        {/* VS Separator */}
        <div className="my-10 mx-4 text-4xl font-bold text-gray-700">
          VS
        </div>
        
        {/* Right column */}
        <div className="flex-1 flex flex-col items-center px-4">
          <DottedCircle>With Visual Copilot</DottedCircle>
            {/* Project Name and/or logo */}
          
          {rightItems.map(item => (
            <ListItem key={item.title} {...item} />
          ))}
        </div>
      </div>
    </section>
  );
};

export default ComparisonChart;
