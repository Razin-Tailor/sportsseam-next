// components/VideoAnalysisGrid.js
"use client";
import { faBasketballBall } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";

const VideoAnalysisGrid = () => {
  const [selectedVideo, setSelectedVideo] = useState(null);

  const videos = [
    {
      id: 1,
      title: "Football Analysis",
      icon: "analysis/nfl.svg",
      videoSrc: "videos/nfl.mp4",
      thumbnail: "analysis/nfl-thumbnail.jpg",
    },
    {
      id: 2,
      title: "Volleyball Analysis",
      icon: "analysis/volleyball.svg",
      videoSrc: "videos/volleyball.mp4",
      thumbnail: "analysis/volleyball-thumbnail.png",
    },
    // ... other videos
  ];

  const handleVideoClick = (video) => {
    setSelectedVideo(video);
  };

  const closeModal = () => {
    setSelectedVideo(null);
  };

  return (
    <div className=" my-8">
      <div className="text-justify mb-10">
        <h2 className="text-4xl md:text-6xl font-light mb-6 text-justify text-white">
          Focus Sports
        </h2>
        <p className="text-lg text-white text-justify">
          We've solved one of the most important problems in computer vision,
          accurately identifying small objects in visual data.
        </p>
        <p className="mt-1 text-lg text-white text-justify">
          Our AI powered copilot is mastering the ability to pinpoint and track
          the interaction of small objects, their motion & interactions in real
          time.
        </p>
        <p className="mt-1 text-lg text-white text-justify">
          Perfect for analyzing almost any dynamic team sport.
        </p>
      </div>
      {/* Grid of video thumbnails */}
      <div className="flex flex-wrap justify-center gap-8">
        {videos.map((video) => (
          <div key={video.id} className="flex-1 min-w-[300px] max-w-[1/3]">
            <div className="flex justify-start items-center">
              <img
                className="w-20 h-20 ml-4 my-2" // Adjust size and margins as needed
                src={video.icon}
                alt={`${video.title} icon`}
              />
              <div
                className="flex-grow relative cursor-pointer pt-[56.25%]"
                onClick={() => handleVideoClick(video)}
              >
                {/* Thumbnail image with lower z-index */}
                <img
                  className="absolute top-0 left-0 w-full h-full object-contain z-10"
                  src={video.thumbnail}
                  alt={video.title}
                />
                {/* Video frame with higher z-index */}
                <img
                  className="absolute top-0 left-0 w-full h-full z-20"
                  src="analysis/videoframe.svg" // Make sure this path is correct and the SVG has transparency
                  alt="Video frame"
                  style={{ pointerEvents: "none" }} // Allows clicking through to the thumbnail
                />
              </div>
            </div>
            <div className="p-2 text-left text-white text-3xl ">
              {video.title}
            </div>
          </div>
        ))}
      </div>
      <div className=" text-white">
        {/* <FontAwesomeIcon
          icon={faBasketballBall}
          width={120}
          height={120}
          className="ml-4 my-2 "
        /> */}
        {/* <div className="ml-4 my-2">
          <h3 className="font-semibold text-xl">Coming Soon</h3>
          <p className="text-lg">Basketball Game Analysis</p>
        </div> */}
      </div>

      {/* Modal for playing selected video */}
      {selectedVideo && (
        <div className="fixed inset-0 z-50 overflow-auto bg-black bg-opacity-75 flex">
          <div className="relative p-2 bg-white w-full max-w-4xl m-auto flex-col flex rounded-lg">
            <video controls autoPlay className="w-full h-full">
              <source src={selectedVideo.videoSrc} type="video/mp4" />
              Your browser does not support the video tag.
            </video>
            <button
              onClick={closeModal}
              className="absolute top-0 right-0 mt-4 mr-4 text-white text-3xl font-bold"
            >
              &times;
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default VideoAnalysisGrid;
