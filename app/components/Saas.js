import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCloud,
  faCloudBolt,
  faCodeBranch,
  faUserCheck,
} from "@fortawesome/free-solid-svg-icons";

const Saas = () => {
  const hoverStyle =
    " transform hover:scale-105 hover:shadow-custom transition duration-300 ease-in-out";
  return (
    <div className="mx-auto p-5">
      <div className="flex flex-col items-start">
        <FontAwesomeIcon
          icon={faCloudBolt}
          width={120}
          height={120}
          className="text-blue-500 mb-4"
        />

        <h2 className="text-4xl md:text-6xl font-light mb-6">
          SaaS-Based, No-Code, Easy to Use
        </h2>
        <p className="text-lg mb-4">
          Our SaaS-based, no-code, highly configurable solution makes it easy to
          analyze video to derive unique insights and improve decision making.
        </p>
      </div>

      <div className="mt-10">
        <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
          <div className={`${hoverStyle}`}>
            <FontAwesomeIcon
              icon={faCodeBranch}
              width={80}
              height={80}
              className={`text-green-500 mb-2`}
            />
            <p className="font-bold">No-Code Solution</p>
            <p className="text-sm">
              Simplify the process with our intuitive interface, requiring no
              technical expertise.
            </p>
          </div>
          <div className={`${hoverStyle}`}>
            <FontAwesomeIcon
              icon={faUserCheck}
              width={80}
              height={80}
              className="text-purple-500 mb-2"
            />
            <p className="font-bold">User-Friendly</p>
            <p className="text-sm">
              Designed for ease of use, enabling knowledge visualists to work
              more efficiently.
            </p>
          </div>
          <div className={`${hoverStyle}`}>
            <FontAwesomeIcon
              icon={faCloud}
              width={80}
              height={80}
              className="text-blue-500 mb-2"
            />
            <p className="font-bold">Cloud-Based</p>
            <p className="text-sm">
              Access powerful computing resources and data storage without
              infrastructure concerns.
            </p>
          </div>
        </div>

        <p className="text-md mt-10">
          We generate results in easy-to-consume visual layouts and structured
          datasets that seamlessly integrate with other analysis tools.
        </p>
      </div>
    </div>
  );
};

export default Saas;
