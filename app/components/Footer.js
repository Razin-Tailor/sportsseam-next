import {
  faFacebook,
  faLinkedin,
  faLinkedinIn,
  faTwitter,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Image from "next/image";
import React from "react";

const Footer = () => {
  return (
    <footer className="bg-gray-950 text-white p-8">
      <div className="container mx-auto flex flex-wrap justify-around">
        <div className="flex w-full md:w-1/4 mb-4 md:mb-0">
          <Image
            className="items-center"
            src={"/logo-thelios.svg"}
            width={90}
            height={90}
            alt="logo"
          />
        </div>
        {/* <div className="w-full md:w-1/4 mb-4 md:mb-0">
                    <h4 className="text-lg font-semibold mb-2">Follow Us</h4>
                    <ul className="flex space-x-4">
                        <li>
                            <a href="#" className="">
                            <FontAwesomeIcon icon={faTwitter} width={20} height={20} />
                            </a>
                        </li>
                        <li>
                            <a href="#" className="">
                            <FontAwesomeIcon icon={faYoutube} width={20} height={20} />
                            </a>
                        </li>
                        <li>
                            <a href="#" className="">
                            <FontAwesomeIcon icon={faLinkedin} width={20} height={20} />
                            </a>
                        </li>
                    </ul>
                </div> */}
        {/* <div className="w-full md:w-1/4 mb-4 md:mb-0">
                    <h4 className="text-lg font-semibold mb-2">Office Hours</h4>
                    <p>Monday - Friday: 9am - 5pm</p>
                </div> */}
        <div className="flex items-center w-full md:w-1/4">
          <p className="text-justify md:text-right">
            &copy; {new Date().getFullYear()} Thelios. All rights reserved.
          </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
