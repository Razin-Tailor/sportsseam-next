import React from "react";
import Image from "next/image";

export function ComingSoonBasketballCard() {
  return (
    <div className="flex flex-row items-center justify-center shadow-lg rounded-lg overflow-hidden">
      <div className="w-3/4 py-8">
        <h1 className="text-3xl md:text-5xl font-light mb-6 text-justify text-white">
          Coming Soon
        </h1>
        <p className="mt-4 text-gray-100 text-2xl font-light">
          Gear up for game changing Basketball Analytics
        </p>
      </div>
      <div className="w-1/4">
        <Image
          src={"/basketball-2.svg"}
          alt="Basketball"
          width={500} // Adjust according to your SVG's aspect ratio
          height={500} // Adjust according to your SVG's aspect ratio
        />
      </div>
    </div>
  );
}

export default ComingSoonBasketballCard;
