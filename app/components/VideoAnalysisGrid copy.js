// components/VideoAnalysisGrid.js
"use client"
import { useState } from 'react';

const VideoAnalysisGrid = () => {
    const [selectedVideo, setSelectedVideo] = useState(null);


    const videos = [
        {
            id: 1,
            title: 'Football Game Analysis',
            icon: 'analysis/nfl.svg',
            videoSrc: 'videos/volleyball.mp4',
            thumbnail: 'fitness.svg'
        },
        {
            id: 2,
            title: 'Volleyball Game Analysis',
            icon: 'analysis/nfl.svg',
            videoSrc: 'videos/volleyball.mp4',
            thumbnail: 'fitness.svg'
        },
        // ... other videos
    ];

    const handleVideoClick = (video) => {
        setSelectedVideo(video);
    };

    const closeModal = () => {
        setSelectedVideo(null);
    };

    return (
        <div className=" my-8">
            <div className="text-justify mb-10">
                <h2 className="text-4xl md:text-6xl font-light mb-10 text-justify text-white">
                    A Breakthrough in Computer Vision
                </h2>
                <p className="text-xl text-white text-justify">
                    We've solved one of the most important problems in computer vision,
                    accurately identifying small objects in visual data.
                </p>
                <p className="mt-2 text-lg text-white text-justify">
                    Our AI powered copilot is mastering the ability to pinpoint and
                    track the interaction of small objects, actions, and humans.
                </p>
            </div>
            {/* Grid of video thumbnails */}
            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-8 justify-center">
                {videos.map((video) => (
                    <div key={video.id} className="group bg-gray-800 rounded-lg overflow-hidden shadow-xl transform transition duration-500 hover:scale-105">
                        <div className="w-full flex justify-between items-center p-4 bg-black bg-opacity-50">
                            <img
                                className="w-20 h-20" // Enlarged icon
                                src={video.icon}
                                alt={`${video.title} icon`}
                            />
                            <span className="text-xl text-white font-semibold">
                                {video.title}
                            </span>
                        </div>
                        <div className="relative cursor-pointer" onClick={() => handleVideoClick(video)}>
                            <img
                                className="w-full h-auto absolute inset-0"
                                src={video.thumbnail}
                                alt={video.title}
                                style={{ clipPath: 'inset(0)' }}
                            />
                            <img
                                className="w-full h-auto"
                                src="analysis/videoframe.svg" // Replace with the actual path to your videoframe SVG
                                alt="Video frame"
                            />
                        </div>
                    </div>
                ))}
            </div>

            {/* Modal for playing selected video */}
            {selectedVideo && (
                <div className="fixed inset-0 z-50 overflow-auto bg-black bg-opacity-75 flex">
                    <div className="relative p-8 bg-white w-full max-w-4xl m-auto flex-col flex rounded-lg">
                        <video controls autoPlay className="w-full">
                            <source src={selectedVideo.videoSrc} type="video/mp4" />
                            Your browser does not support the video tag.
                        </video>
                        <button
                            onClick={closeModal}
                            className="absolute top-0 right-0 mt-4 mr-4 text-black text-3xl font-bold"
                        >
                            &times;
                        </button>
                    </div>
                </div>
            )}
        </div>
    );
};

export default VideoAnalysisGrid;

// const videos = [
//     {
//       id: 1,
//       title: 'Football Game Analysis',
//       icon: 'analysis/nfl.svg',
//       videoSrc: 'videos/volleyball.mp4',
//       thumbnail: 'fitness.svg'
//     },
//     {
//       id: 2,
//       title: 'Volleyball Game Analysis',
//       icon: 'analysis/nfl.svg',
//       videoSrc: 'videos/volleyball.mp4',
//       thumbnail: 'fitness.svg'
//     },
//     // ... other videos
//   ];