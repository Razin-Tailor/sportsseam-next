import Image from 'next/image';
import React from 'react';

const Overview = () => {
    return (
        <section className='flex flex-col justify-between'>
            <div className='text-justify pb-10'>
                <p className='text-4xl md:text-6xl font-light mb-10 text-justify'>Knowledge Visualists</p>
                <p className='font-thin italic'>Professionals who analyze real-world visual data to make decisions</p>
            </div>
            <hr/>
            <div className='flex flex-col items-center text-justify px-4 py-20 md:flex-row md:text-left md:items-center md:justify-between  md:rounded-lg shadow-xl'>

                <div className='mb-8 md:mb-0 md:w-1/2 py-6'>
                    <h2 className='text-4xl md:text-6xl font-light mb-10'>Empowering Knowledge Visualists</h2>
                    <p className='text-white font-light text-lg text-justify'>From a sports coach analyzing game film to a doctor performing a medical procedure, we help <span className='font-bold'>“knowledge visualists”</span> analyze visual data with greater speed and accuracy to make better decisions.</p>
                    <br/>
                    <p className='text-white font-light text-lg text-justify'>We’re currently working with multiple professional sports organizations and top healthcare institutions</p>
                </div>
                <div className='relative md:w-1/2 flex justify-center md:justify-end md:pr-16'>
                    {/* Main image container to position satellite images in relation */}
                    <div className='relative w-full flex justify-center pt-16 pb-16'>
                        {/* Central image */}
                        <div className='w-40 h-40 md:w-60 md:h-60 bg-gray-600 bg-opacity-45 rounded-full'>
                            <Image
                                src={'/analytics.svg'}
                                alt="Analytics"
                                layout='responsive'
                                width={240}
                                height={240}
                                objectFit='contain'
                            />
                        </div>
                        {/* Top-left satellite image, closer to the central image on larger screens */}
                        <div className='absolute top-0 left-1/4 transform -translate-x-1/2 -translate-y-1/2 w-20 h-20 md:w-28 md:h-28 bg-gray-600 bg-opacity-25 rounded-full'>
                            <Image
                                src={'/fitness.svg'}
                                alt="Fitness"
                                layout='responsive'
                                width={112}
                                height={112}
                                objectFit='contain'
                            />
                        </div>
                        {/* Bottom-right satellite image, closer to the central image on larger screens */}
                        <div className='absolute bottom-0 right-1/4 transform translate-x-1/2 translate-y-1/2 w-20 h-20 md:w-28 md:h-28 bg-gray-600 bg-opacity-25 rounded-full'>
                            <Image
                                src={'/medicine.svg'}
                                alt="Medicine"
                                layout='responsive'
                                width={112}
                                height={112}
                                objectFit='contain'
                            />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Overview;
