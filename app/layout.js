import { Inter } from "next/font/google";
import "./globals.css";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import { BackgroundBeams } from "./components/ui/background-beams";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Thelios AI",
  description: "AI-powered Computer Vision to transform decision making",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className="bg-gradient-to-b from-gray-800 to-black ">
        <Navbar className="z-10" />
        {children}
        <Footer />
      </body>
    </html>
  );
}
